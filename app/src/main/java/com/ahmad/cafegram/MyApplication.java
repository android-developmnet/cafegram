package com.ahmad.cafegram;

import android.app.Application;

import com.ahmad.cafegram.utills.Utils;

/**
 * Created by ahmad on 1/20/16.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Utils.setupUtils(getApplicationContext());
    }
}
