package com.ahmad.cafegram.common;

public class Constants {
    public static final String CLIENT_ID = "1290ce23b8dc4dae921c0a8ccc6c9d67";
    public static final String CLIENT_SECRET = "16366f561a014752be2e38f321c3d0bc";
    public static final String REDIRECT_URI = "http://piccolo.ir/";
    public static final String GRANT_TYPE = "authorization_code";

    public static final String BASE_INSTAGRAM_URL = "https://api.instagram.com/";

    public static final String LOGIN_URL = BASE_INSTAGRAM_URL + "oauth/authorize/" +
            "?client_id=" + CLIENT_ID + "&redirect_uri=" + REDIRECT_URI
            + "&response_type=code";


    public static final String PREF_ACCESS_TOKEN = "s1";
    public static final String PREF_USER_ID = "s2";
    public static final String PREF_USER_NAME = "s3";
    public static final String PREF_FULL_NAME = "s4";
    public static final String PREF_PROFILE_IMAGE = "s5";
    public static final String EXTRA_USER = "s6";
    public static final String BUNDLE_RECYCLER_TYPE = "s7";
}
