package com.ahmad.cafegram.common;

/**
 * Created by ahmad on 1/22/16.
 */
public interface OnRecyclerItemClickListener {
    void onItemClick(int position, Object object);
}
