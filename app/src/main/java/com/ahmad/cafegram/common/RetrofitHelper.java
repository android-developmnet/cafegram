package com.ahmad.cafegram.common;

import com.ahmad.cafegram.BuildConfig;
import com.ahmad.cafegram.model.APIService;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RetrofitHelper {
    private static RetrofitHelper ourInstance = new RetrofitHelper();
    private final APIService apiService;

    public static RetrofitHelper getInstance() {
        return ourInstance;
    }

    private RetrofitHelper() {
        Retrofit retrofit = getRetrofit(Constants.BASE_INSTAGRAM_URL);
        apiService = retrofit.create(APIService.class);
    }

    public APIService getService() {
        return apiService;
    }

    private Retrofit getRetrofit(String baseUrl) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient();
        if (BuildConfig.DEBUG)
            client.interceptors().add(logging);
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
