package com.ahmad.cafegram.common;

/**
 * Created by ahmad on 1/24/16.
 */
public interface RetryListener {
    void onRetry();
}
