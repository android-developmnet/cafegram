package com.ahmad.cafegram.model;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by ahmad on 1/20/16.
 */
public interface APIService {
    @FormUrlEncoded
    @POST("oauth/access_token")
    Call<AuthModel> getAccessToken(@Field("client_id") String client_id,
                                   @Field("client_secret") String client_secret,
                                   @Field("grant_type") String grant_type,
                                   @Field("redirect_uri") String redirect_uri,
                                   @Field("code") String code);

    @GET("v1/users/self/media/recent/")
    Call<BaseArrayModel<PostModel>> fetchRecentMedia(@Query("access_token") String access_token);

    @GET("v1/users/self")
    Call<BaseObjectModel<UserModel>> fetchUserProfile(@Query("access_token") String access_token);
}
