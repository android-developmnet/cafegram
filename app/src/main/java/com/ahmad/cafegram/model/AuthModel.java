package com.ahmad.cafegram.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmad on 1/20/16.
 */
public class AuthModel {
    @SerializedName("access_token")
    private String accessToken;
    private UserModel user;

    public String getAccessToken() {
        return accessToken;
    }

    public UserModel getUserModel() {
        return user;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
}
