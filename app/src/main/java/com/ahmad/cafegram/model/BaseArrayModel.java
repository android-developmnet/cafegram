package com.ahmad.cafegram.model;

import java.util.List;

/**
 * Created by ahmad on 1/20/16.
 */
public class BaseArrayModel<P> {
    private MetaModel meta;
    private List<P> data;

    public MetaModel getMeta() {
        return meta;
    }

    public List<P> getData() {
        return data;
    }
}
