package com.ahmad.cafegram.model;

/**
 * Created by ahmad on 1/20/16.
 */
public class BaseObjectModel<P> {
    private MetaModel meta;
    private P data;

    public MetaModel getMeta() {
        return meta;
    }

    public P getData() {
        return data;
    }
}
