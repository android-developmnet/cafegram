package com.ahmad.cafegram.model;

/**
 * Created by ahmad on 1/20/16.
 */
public class CaptionModel {
    private long id;
    private  String text;

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }
}
