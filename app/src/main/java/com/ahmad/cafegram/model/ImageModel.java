package com.ahmad.cafegram.model;

/**
 * Created by ahmad on 1/20/16.
 */
public class ImageModel {
    private String url;
    private int width;
    private int height;

    public String getUrl() {
        return url;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
