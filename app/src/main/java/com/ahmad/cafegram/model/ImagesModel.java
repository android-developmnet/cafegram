package com.ahmad.cafegram.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmad on 1/20/16.
 */
public class ImagesModel {
    @SerializedName("low_resolution")
    private ImageModel lowResolution;
    @SerializedName("standard_resolution")
    private ImageModel standardResolution;

    private ImageModel thumbnail;


    public ImageModel getLowResolution() {
        return lowResolution;
    }

    public ImageModel getThumbnail() {
        return thumbnail;
    }

    public ImageModel getStandardResolution() {
        return standardResolution;
    }
}
