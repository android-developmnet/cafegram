package com.ahmad.cafegram.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmad on 1/20/16.
 */
public class MetaModel {
    @SerializedName("code")
    private int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
}
