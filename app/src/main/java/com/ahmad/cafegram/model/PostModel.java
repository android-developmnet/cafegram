package com.ahmad.cafegram.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmad on 1/20/16.
 */
public class PostModel extends BaseArrayModel<PostModel> {
    @SerializedName("created_time")
    private long createdTime;
    @SerializedName("user_has_liked")
    private boolean userHasLiked;
    private CaptionModel caption;
    private ImagesModel images;
    private UserModel user;
    private CountModel likes;
    private CountModel comments;


    public long getCreatedTime() {
        return createdTime;
    }

    public boolean isUserHasLiked() {
        return userHasLiked;
    }

    public CaptionModel getCaption() {
        return caption;
    }

    public ImagesModel getImages() {
        return images;
    }

    public UserModel getUser() {
        return user;
    }

    public CountModel getLikes() {
        return likes;
    }

    public CountModel getComments() {
        return comments;
    }
}
