package com.ahmad.cafegram.model;

import android.support.v4.app.Fragment;

/**
 * Created by ahamdUser on 2/12/2015.
 */
public class TabModel {
    private int resId;
    private Fragment fragment;

    public TabModel(int title, Fragment fragment) {
        this.resId = title;
        this.fragment = fragment;
    }

    public int getResId() {
        return this.resId;
    }

    public Fragment getFragment() {
        return fragment;
    }
}
