package com.ahmad.cafegram.model;

import android.text.TextUtils;

import com.ahmad.cafegram.common.Constants;
import com.ahmad.cafegram.utills.Base64Helper;
import com.ahmad.cafegram.utills.SPref;
import com.ahmad.cafegram.utills.Utils;

/**
 * Created by ahmad on 1/20/16.
 */
public class User {
    private static User ourInstance = new User();
    private AuthModel authModel = new AuthModel();

    public static User getInstance() {
        return ourInstance;
    }

    private User() {
        retrieveDataFromCache();
    }


    public AuthModel getAuthModel() {
        return authModel;
    }

    public void setAuthModel(AuthModel authModel) {
        this.authModel = authModel;
        storeDataToCache(authModel);
    }

    private void storeDataToCache(AuthModel authModel) {
        SPref.getInstance().storeValue(Constants.PREF_FULL_NAME, authModel.getUserModel().getFullName());
        SPref.getInstance().storeValue(Constants.PREF_PROFILE_IMAGE, authModel.getUserModel().getProfilePicture());
        SPref.getInstance().storeValue(Constants.PREF_USER_NAME, authModel.getUserModel().getUsername());
        SPref.getInstance().storeValue(Constants.PREF_USER_ID, authModel.getUserModel().getId());
        SPref.getInstance().storeValue(Constants.PREF_ACCESS_TOKEN,
                Base64Helper.encryptAndBase64Encode(authModel.getAccessToken(), Base64Helper.getDay()));

    }

    private void retrieveDataFromCache() {

        String token = SPref.getInstance().retrieveStringValue(Constants.PREF_ACCESS_TOKEN);
        if (!TextUtils.isEmpty(token)) {
            authModel.setAccessToken(Base64Helper.base64DecodeAndDecrypt(token, Base64Helper.getDay()));
            UserModel userModel = new UserModel();
            userModel.setFullName(SPref.getInstance().retrieveStringValue(Constants.PREF_FULL_NAME));
            userModel.setId(SPref.getInstance().retrieveLongValue(Constants.PREF_USER_ID));
            userModel.setUsername(SPref.getInstance().retrieveStringValue(Constants.PREF_USER_NAME));
            userModel.setProfilePicture(SPref.getInstance().retrieveStringValue(Constants.PREF_PROFILE_IMAGE));
            authModel.setUser(userModel);
        }

    }
    // Clear all user data include web view cookies
    public void clearUserData() {
        authModel = new AuthModel();
        Utils.clearCacheFolder();
        SPref.getInstance().removeAll();
        Utils.clearCookies();
    }
}
