package com.ahmad.cafegram.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmad on 1/22/16.
 */
public class UserInfoModel {
    private int media;
    @SerializedName("followed_by")
    private int followedBy;
    private int follows;

    public int getMedia() {
        return media;
    }

    public int getFollowedBy() {
        return followedBy;
    }

    public int getFollows() {
        return follows;
    }
}
