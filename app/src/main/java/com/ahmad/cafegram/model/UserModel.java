package com.ahmad.cafegram.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmad on 1/20/16.
 */
public class UserModel implements Parcelable {

    private long id;
    private String username;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("profile_picture")
    private String profilePicture;
    @SerializedName("counts")
    private UserInfoModel userInfo;
    private String bio;
    private String website;

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return fullName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }


    public UserInfoModel getUserInfo() {
        return userInfo;
    }

    public String getBio() {
        return bio;
    }

    public String getWebsite() {
        return website;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.username);
        dest.writeString(this.fullName);
        dest.writeString(this.profilePicture);
    }

    public UserModel() {

    }

    protected UserModel(Parcel in) {
        this.id = in.readLong();
        this.username = in.readString();
        this.fullName = in.readString();
        this.profilePicture = in.readString();
    }

    public static final Parcelable.Creator<UserModel> CREATOR = new Parcelable.Creator<UserModel>() {
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
