package com.ahmad.cafegram.presenter.feed;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmad.cafegram.R;
import com.ahmad.cafegram.common.OnRecyclerItemClickListener;
import com.ahmad.cafegram.common.RecyclerType;
import com.ahmad.cafegram.model.PostModel;
import com.ahmad.cafegram.utills.TimeUtils;
import com.ahmad.cafegram.utills.Utils;
import com.ahmad.cafegram.widget.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {
    private final List<PostModel> posts;
    private OnRecyclerItemClickListener listener;
    private final int NORMAL = 0;
    private final int LIKED = 1;
    private RecyclerType currentType;

    public FeedAdapter(List<PostModel> posts, RecyclerType recyclerType) {
        this.posts = posts;
        this.currentType = recyclerType;
    }

    @Override
    public int getItemViewType(int position) {
        return posts.get(position).isUserHasLiked() ? LIKED : NORMAL;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        if (currentType == RecyclerType.LIST)
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_post_list, viewGroup, false);
        else
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_post_grid, viewGroup, false);

        return new ViewHolder(view);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onBindViewHolder(FeedAdapter.ViewHolder holder, int position) {
        PostModel post = posts.get(position);
        Picasso.with(Utils.getAppContext()).load(post.getImages().getStandardResolution().getUrl())
                .placeholder(R.drawable.placeholder).into(holder.image);
        if (currentType == RecyclerType.LIST) {
            Picasso.with(Utils.getAppContext()).load(post.getUser().getProfilePicture())
                    .transform(new CircleTransform()).into(holder.profileImage);
            if (post.getCaption() != null)
                holder.caption.setText(post.getCaption().getText());
            holder.userName.setText(post.getUser().getUsername());
            holder.postTime.setText(TimeUtils.getTimeAgoEnNumber(post.getCreatedTime()));
            holder.likeCount.setText(post.getLikes().getCount());
            holder.commentCount.setText(post.getComments().getCount());
            if (getItemViewType(position) == LIKED) {
                holder.favoriteIcon.setImageResource(R.drawable.ic_favorite_red_24dp);
            }
        }

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.itemPostImage)
        ImageView image;
        @Nullable
        @Bind(R.id.itemPostCaption)
        TextView caption;
        @Nullable
        @Bind(R.id.itemPostProfileImage)
        ImageView profileImage;
        @Nullable
        @Bind(R.id.itemPostUserName)
        TextView userName;
        @Nullable
        @Bind(R.id.itemPostTime)
        TextView postTime;
        @Nullable
        @Bind(R.id.itemPostLikeCount)
        TextView likeCount;
        @Nullable
        @Bind(R.id.itemPostCommentCount)
        TextView commentCount;
        @Nullable
        @Bind(R.id.itemPostFavoriteIcon)
        ImageView favoriteIcon;

        public ViewHolder(final View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
        @Nullable
        @OnClick(R.id.itemPostUserSection)
        void onClick() {
            if (listener != null) {
                listener.onItemClick(getLayoutPosition(), posts.get(getLayoutPosition()).getUser());
            }
        }
    }

    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener listener) {
        this.listener = listener;
    }
}
