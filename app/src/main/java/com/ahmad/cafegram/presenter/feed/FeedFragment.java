package com.ahmad.cafegram.presenter.feed;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ahmad.cafegram.R;
import com.ahmad.cafegram.common.Constants;
import com.ahmad.cafegram.common.OnRecyclerItemClickListener;
import com.ahmad.cafegram.common.RecyclerType;
import com.ahmad.cafegram.common.RetryListener;
import com.ahmad.cafegram.model.PostModel;
import com.ahmad.cafegram.model.User;
import com.ahmad.cafegram.model.UserModel;
import com.ahmad.cafegram.presenter.profile.ProfileActivity;
import com.ahmad.cafegram.utills.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class FeedFragment extends Fragment implements IFeedView,
        SwipeRefreshLayout.OnRefreshListener, OnRecyclerItemClickListener, RetryListener {
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.feed)
    RecyclerView feedRecyclerView;
    @Bind(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    private FeedPresenter presenter;
    private RecyclerType recyclerType;

    public static FeedFragment newInstance() {
        return new FeedFragment();
    }

    public FeedFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            recyclerType = (RecyclerType) getArguments().getSerializable(Constants.BUNDLE_RECYCLER_TYPE);
        else
            recyclerType = RecyclerType.LIST;
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.bind(this, view);
        initUI();
        presenter = new FeedPresenter(this);
        presenter.fetchRecentMedia(false);
        return view;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.actionProfile) {
            startProfileActivity(User.getInstance().getAuthModel().getUserModel());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initUI() {
        feedRecyclerView.setLayoutManager(Utils.getLayoutManger(getActivity(), recyclerType));
        feedRecyclerView.setHasFixedSize(true);
        //Set empty adapter for fix swipe refresh layout problem
        feedRecyclerView.setAdapter(new FeedAdapter(new ArrayList<PostModel>(), recyclerType));
        swipeRefreshLayout.setOnRefreshListener(this);


    }

    @Override
    public void OnDataLoaded(List<PostModel> posts) {
        FeedAdapter feedAdapter = new FeedAdapter(posts, recyclerType);
        feedRecyclerView.setAdapter(feedAdapter);
        feedAdapter.setOnRecyclerItemClickListener(this);

    }

    @Override
    public void showHideProgress(boolean show, boolean fromRefresh) {
        progressBar.setVisibility(show && !fromRefresh ? View.VISIBLE : View.GONE);
        swipeRefreshLayout.setRefreshing(show);
    }

    @Override
    public void onRequestFailed(String message) {
        Snackbar.make(getActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
        swipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onRefresh() {
        presenter.fetchRecentMedia(true);
    }


    @Override
    public void onItemClick(int position, Object object) {
        startProfileActivity((UserModel) object);
    }

    private void startProfileActivity(UserModel userModel) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.putExtra(Constants.EXTRA_USER, userModel);
        startActivity(intent);
    }


    @Override
    public void onRetry() {
        presenter.fetchRecentMedia(false);
    }
}
