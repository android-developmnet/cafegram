package com.ahmad.cafegram.presenter.feed;

import com.ahmad.cafegram.R;
import com.ahmad.cafegram.common.RetrofitHelper;
import com.ahmad.cafegram.utills.L;
import com.ahmad.cafegram.model.BaseArrayModel;
import com.ahmad.cafegram.model.PostModel;
import com.ahmad.cafegram.model.User;
import com.ahmad.cafegram.utills.Utils;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class FeedPresenter {
    private final IFeedView iView;

    public FeedPresenter(IFeedView iView) {
        this.iView = iView;
    }

    public void fetchRecentMedia(final boolean fromRefresh) {
        if (!Utils.isNetworkAvailable()) {
            iView.onRequestFailed(Utils.getStringResource(R.string.network_error));
            return;
        }
        iView.showHideProgress(true, fromRefresh);

        Call<BaseArrayModel<PostModel>> call = RetrofitHelper.getInstance().getService()
                .fetchRecentMedia(User.getInstance().getAuthModel().getAccessToken());
        call.enqueue(new Callback<BaseArrayModel<PostModel>>() {
            @Override
            public void onResponse(Response<BaseArrayModel<PostModel>> response, Retrofit retrofit) {
                if (response.body().getMeta().getStatusCode() == 200) {
                    iView.OnDataLoaded(response.body().getData());
                } else {
                    iView.onRequestFailed(Utils.getStringResource(R.string.request_failed));
                }
                iView.showHideProgress(false, fromRefresh);
            }

            @Override
            public void onFailure(Throwable t) {
                iView.showHideProgress(false, fromRefresh);
                iView.onRequestFailed(Utils.getStringResource(R.string.request_failed));
                L.e(t.getMessage());
            }
        });
    }
}

