package com.ahmad.cafegram.presenter.feed;

import com.ahmad.cafegram.model.PostModel;

import java.util.List;

/**
 * Created by ahmad on 1/20/16.
 */
public interface IFeedView {
    void OnDataLoaded(List<PostModel> posts);

    void showHideProgress(boolean show , boolean fromRefresh);

    void onRequestFailed(String message);

}
