package com.ahmad.cafegram.presenter.login;

import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ahmad.cafegram.common.Constants;
import com.ahmad.cafegram.R;
import com.ahmad.cafegram.utills.L;
import com.ahmad.cafegram.utills.Utils;

/**
 * Created by ahmad on 1/20/16.
 */
public class AuthWebViewClient extends WebViewClient {
    private final ILoginView iView;
    private boolean authorizationSuccess;

    public AuthWebViewClient(ILoginView view) {
        this.iView = view;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith(Constants.REDIRECT_URI)) {
            Uri uri = Uri.parse(url);
            if (uri.getQueryParameter("error") != null) {
                iView.displayError(Utils.getStringResource(R.string.auth_error));
                L.e(url);
            } else {
                authorizationSuccess = true;
                iView.userAuthorized(uri.getQueryParameter("code"));
            }
            iView.showHideWebView(false);
            return true;
        }
        return false;

    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        iView.showHideProgress(true);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (!authorizationSuccess)
            iView.showHideProgress(false);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        iView.showHideWebView(false);
        iView.displayError(Utils.getStringResource(R.string.auth_error));
    }
}
