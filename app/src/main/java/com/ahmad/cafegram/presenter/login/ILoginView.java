package com.ahmad.cafegram.presenter.login;

/**
 * Created by ahmad on 1/20/16.
 */
public interface ILoginView {
    void showHideProgress(boolean show);

    void showHideWebView(boolean show);

    void displayError(String message);

    void userAuthorized(String code);

    void onGetAccessTokenSuccess();

    void onNetworkUnAvailable();
}
