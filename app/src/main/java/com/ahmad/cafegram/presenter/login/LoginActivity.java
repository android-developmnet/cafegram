package com.ahmad.cafegram.presenter.login;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;

import com.ahmad.cafegram.R;
import com.ahmad.cafegram.presenter.feed.FeedActivity;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements ILoginView {
    @Bind(R.id.loginRoot)
    View root;
    @Bind(R.id.authWebView)
    WebView authWebView;
    @Bind(R.id.loginSection)
    View loginSection;
    @BindString(R.string.waiting)
    String waiting;
    @BindString(R.string.error_title)
    String errorTitle;

    private LoginPresenter presenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        presenter = new LoginPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(waiting);

    }


    public void onClickLogin(View view) {
        presenter.startAuthentication(authWebView);
    }

    @Override
    public void showHideProgress(boolean show) {
        if (show)
            progressDialog.show();
        else
            progressDialog.dismiss();
    }

    @Override
    public void showHideWebView(boolean show) {
        authWebView.setVisibility(show ? View.VISIBLE : View.GONE);
        loginSection.setVisibility(!show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void displayError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(errorTitle);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void userAuthorized(String code) {
        presenter.fetchAccessToken(code);
    }

    @Override
    public void onGetAccessTokenSuccess() {
        Intent intent = new Intent(this, FeedActivity.class);
        startActivity(intent);
    }

    @Override
    public void onNetworkUnAvailable() {
        Snackbar.make(root, R.string.network_error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        if (authWebView.getVisibility() == View.VISIBLE) {
            showHideWebView(false);
        } else
            super.onBackPressed();
    }
}
