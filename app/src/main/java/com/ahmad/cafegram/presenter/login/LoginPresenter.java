package com.ahmad.cafegram.presenter.login;

import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;

import com.ahmad.cafegram.R;
import com.ahmad.cafegram.common.Constants;
import com.ahmad.cafegram.common.RetrofitHelper;
import com.ahmad.cafegram.model.AuthModel;
import com.ahmad.cafegram.model.User;
import com.ahmad.cafegram.utills.L;
import com.ahmad.cafegram.utills.Utils;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by ahmad on 1/20/16.
 */
public class LoginPresenter {

    private final ILoginView iView;

    public LoginPresenter(ILoginView iView) {
        this.iView = iView;
        // Check if access token already exist skip login process
        if (!TextUtils.isEmpty(User.getInstance().getAuthModel().getAccessToken())) {
            iView.onGetAccessTokenSuccess();
        }
    }

    public void startAuthentication(WebView authWebView) {
        if (!Utils.isNetworkAvailable()) {
            iView.onNetworkUnAvailable();
            return;
        }
        // Set on touch for fix keyboard not showing up in older device
        authWebView.requestFocus(View.FOCUS_DOWN);
        authWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });
        authWebView.requestFocusFromTouch();
        authWebView.setVerticalScrollBarEnabled(false);
        authWebView.setHorizontalScrollBarEnabled(false);
        authWebView.setWebViewClient(new AuthWebViewClient(iView));
        authWebView.getSettings().setJavaScriptEnabled(true);
        authWebView.loadUrl(Constants.LOGIN_URL);
        iView.showHideWebView(true);
    }

    public void fetchAccessToken(String code) {
        if (!Utils.isNetworkAvailable()) {
            iView.onNetworkUnAvailable();
            return;
        }
        iView.showHideProgress(true);
        Call<AuthModel> call = RetrofitHelper.getInstance().getService()
                .getAccessToken(Constants.CLIENT_ID, Constants.CLIENT_SECRET,
                        Constants.GRANT_TYPE, Constants.REDIRECT_URI, code);
        call.enqueue(new Callback<AuthModel>() {
            @Override
            public void onResponse(Response<AuthModel> response, Retrofit retrofit) {
                User.getInstance().setAuthModel(response.body());
                iView.onGetAccessTokenSuccess();
                iView.showHideProgress(false);
            }

            @Override
            public void onFailure(Throwable t) {
                iView.showHideProgress(false);
                iView.displayError(Utils.getStringResource(R.string.auth_error));
                L.e(t.getMessage());
            }
        });
    }
}
