package com.ahmad.cafegram.presenter.profile;

import com.ahmad.cafegram.model.UserModel;

/**
 * Created by ahmad on 1/22/16.
 */
public interface IProfileView {

    void createTab();

    void setUserProfileData();

    void OnDataLoaded(UserModel userModel);

}
