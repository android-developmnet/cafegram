package com.ahmad.cafegram.presenter.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmad.cafegram.R;
import com.ahmad.cafegram.common.Constants;
import com.ahmad.cafegram.model.User;
import com.ahmad.cafegram.model.UserModel;
import com.ahmad.cafegram.presenter.login.LoginActivity;
import com.ahmad.cafegram.widget.CircleTransform;
import com.ahmad.cafegram.widget.VerticalTextView;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity implements IProfileView {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.headerProfileImage)
    ImageView profileImage;
    @Bind(R.id.headerProfileFullName)
    TextView profileFullName;
    @Bind(R.id.headerProfileBio)
    TextView profileBio;
    @Bind(R.id.headerProfilePosts)
    VerticalTextView profilePosts;
    @Bind(R.id.headerProfileFollowing)
    VerticalTextView profileFollowing;
    @Bind(R.id.headerProfileFollowers)
    VerticalTextView profileFollowers;
    @Bind(R.id.profileViewPager)
    ViewPager profileViewPager;
    @Bind(R.id.profileTab)
    TabLayout profileTab;
    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        userModel = getIntent().getParcelableExtra(Constants.EXTRA_USER);
        initUI();
        ProfilePresenter presenter = new ProfilePresenter(this);
        presenter.fetchProfileDetails();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.actionLogout) {
            User.getInstance().clearUserData();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUI() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }


    @Override
    public void OnDataLoaded(UserModel userModel) {
        this.userModel = userModel;
        profileFollowers.setFirstText("" + userModel.getUserInfo().getFollowedBy());
        profileFollowing.setFirstText("" + userModel.getUserInfo().getFollows());
        profilePosts.setFirstText("" + userModel.getUserInfo().getMedia());
        profileBio.setText(userModel.getBio());
    }

    @Override
    public void createTab() {
        ProfilePagerAdapter pagerAdapter = new ProfilePagerAdapter(getSupportFragmentManager());
        profileViewPager.setAdapter(pagerAdapter);
        profileTab.setupWithViewPager(profileViewPager);
        if (profileTab.getTabAt(0) != null && profileTab.getTabAt(1) != null) {
            profileTab.getTabAt(0).setIcon(R.drawable.ic_action_view_grid);
            profileTab.getTabAt(1).setIcon(R.drawable.ic_action_view_list);
        }
    }

    @Override
    public void setUserProfileData() {
        Picasso.with(this).load(userModel.getProfilePicture()).transform(new CircleTransform()).into(profileImage);
        profileFullName.setText(userModel.getFullName());

    }


}
