package com.ahmad.cafegram.presenter.profile;

/**
 * Created by ahamdUser on 10/25/2014.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ahmad.cafegram.common.Constants;
import com.ahmad.cafegram.R;
import com.ahmad.cafegram.common.RecyclerType;
import com.ahmad.cafegram.model.TabModel;
import com.ahmad.cafegram.presenter.feed.FeedFragment;

import java.util.ArrayList;

/**
 * Created by ahamdUser on 10/24/2014.
 */
public class ProfilePagerAdapter extends FragmentPagerAdapter {
    private ArrayList<TabModel> tabs;

    public ProfilePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        createTabs();
    }

    private void createTabs() {
        tabs = new ArrayList<>();
        tabs.add(new TabModel(R.drawable.ic_action_view_grid, createFragment(RecyclerType.GRID)));
        tabs.add(new TabModel(R.drawable.ic_action_view_list, createFragment(RecyclerType.LIST)));

    }

    private Fragment createFragment(RecyclerType recyclerType) {
        FeedFragment fragment = FeedFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_RECYCLER_TYPE, recyclerType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public Fragment getItem(int position) {

        return tabs.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return tabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
