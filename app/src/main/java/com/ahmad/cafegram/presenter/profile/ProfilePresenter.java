package com.ahmad.cafegram.presenter.profile;

import com.ahmad.cafegram.common.RetrofitHelper;
import com.ahmad.cafegram.model.BaseObjectModel;
import com.ahmad.cafegram.model.User;
import com.ahmad.cafegram.model.UserModel;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by ahmad on 1/22/16.
 */
public class ProfilePresenter {

    private final IProfileView iView;

    public ProfilePresenter(IProfileView iView) {
        this.iView = iView;
        this.iView.createTab();
        this.iView.setUserProfileData();
    }

    public void fetchProfileDetails() {
        Call<BaseObjectModel<UserModel>> call = RetrofitHelper.getInstance().getService()
                .fetchUserProfile(User.getInstance().getAuthModel().getAccessToken());
        call.enqueue(new Callback<BaseObjectModel<UserModel>>() {
            @Override
            public void onResponse(Response<BaseObjectModel<UserModel>> response, Retrofit retrofit) {
                iView.OnDataLoaded(response.body().getData());
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }


    public void setTab() {

    }
}
