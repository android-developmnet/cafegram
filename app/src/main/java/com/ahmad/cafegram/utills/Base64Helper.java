package com.ahmad.cafegram.utills;

import android.util.Base64;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by ahamdUser on 11/4/2014.
 */
public class Base64Helper {
    // Encryption helper methods

    private static String swapEach2CharsInString(String s)
    {
        char[] chars = new char[16];
        for (int i = 0; i < 16; i += 2)
        {
            chars[i] = s.charAt(i + 1);
            chars[i + 1] = s.charAt(i);
        }
        return String.valueOf(chars);
    }

    // unscrambled api key is 80ffe14b933bffcd84cfae37f1d98bdf
    private String getDate()
    {
        char[] chars = { '8', '0', 'f', 'f', 'e', '1', '4', 'b', '9', '3', '3', 'b', 'f', 'f', 'c', 'd', '8', '4', 'c', 'f', 'a', 'e', '3', '7', 'f', '1', 'd', '9', '8', 'b', 'd', 'f' };
        return String.valueOf(chars);
    }

    // unscrambled api pass is u4jdi3a93lt4v6jd
    public static  String getTime()
    {
        String key = "4udj3i9al34t6vdj";
        return swapEach2CharsInString(key);
    }

    // unscrambled key for in-app use oxcazw6u96zlg7c8
    public static  String getDay()
    {
        String key = "xoacwzu669lz7g8c";
        return swapEach2CharsInString(key);
    }

    public static String encryptAndBase64Encode(String dataToEncrypt, String key)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding");
            Key sKey = new SecretKeySpec(key.getBytes(), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, sKey);
            byte[] encryptedBytes = cipher.doFinal(dataToEncrypt.getBytes());
            return Base64.encodeToString(encryptedBytes, Base64.DEFAULT);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static String base64DecodeAndDecrypt(String dataToDecrypt, String key)
    {
        try
        {
            byte[] decodedBytes = Base64.decode(dataToDecrypt, Base64.DEFAULT);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding");
            Key sKey = new SecretKeySpec(key.getBytes(), "AES");
            cipher.init(Cipher.DECRYPT_MODE, sKey);
            byte[] decryptedBytes = cipher.doFinal(decodedBytes);
            return new String(decryptedBytes);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

}
