package com.ahmad.cafegram.utills;

import android.util.Log;

import com.ahmad.cafegram.BuildConfig;

public class L {

    public static void v() {
        if (BuildConfig.DEBUG) {
            Log.v(ClassInfo.outerClassName() + "#" + ClassInfo.outerMethodName(), " ");
        }
    }

    public static void v(String message) {
        if (BuildConfig.DEBUG) {
            if (message == null) {
                message = " ";
            }
            Log.v(ClassInfo.outerClassName() + "#" + ClassInfo.outerMethodName(), message);
        }
    }

    public static void e() {
        if (BuildConfig.DEBUG) {
            Log.e(ClassInfo.outerClassName() + "#" + ClassInfo.outerMethodName(), " ");
        }
    }

    public static void e(String message) {
        if (BuildConfig.DEBUG) {
            if (message == null) {
                message = " ";
            }
            Log.e(ClassInfo.outerClassName() + "#" + ClassInfo.outerMethodName(), message);
        }
    }

    public static void i() {
        if (BuildConfig.DEBUG) {
            Log.i(ClassInfo.outerClassName() + "#" + ClassInfo.outerMethodName(), " ");
        }
    }

    public static void i(String message) {
        if (BuildConfig.DEBUG) {
            if (message == null) {
                message = " ";
            }
            Log.i(ClassInfo.outerClassName() + "#" + ClassInfo.outerMethodName(), message);
        }
    }

    private static class ClassInfo {
        private static final int CLIENT_CODE_STACK_INDEX;

        static {
            // Finds out the index of "this code" in the returned stack trace - funny but it differs in JDK 1.5 and 1.6
            int i = 0;
            for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                i++;
                if (ste.getClassName().equals(ClassInfo.class.getName())) {
                    break;
                }
            }
            CLIENT_CODE_STACK_INDEX = i;
        }

        public static String outerMethodName() {
            return Thread.currentThread().getStackTrace()[CLIENT_CODE_STACK_INDEX + 1].getMethodName();
        }

        public static String outerClassName() {
            String fullClass = Thread.currentThread().getStackTrace()[CLIENT_CODE_STACK_INDEX + 1].getClassName();
            String[] components = fullClass.split("\\.");
            return components[components.length - 1];
        }
    }

}
