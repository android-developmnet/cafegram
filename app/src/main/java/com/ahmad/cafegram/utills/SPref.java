package com.ahmad.cafegram.utills;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ahamd on 2/11/2015.
 */
public class SPref {

    private static SPref instance;
    private final SharedPreferences.Editor editor;
    private final SharedPreferences prefs;

    private SPref() {
        prefs = Utils.getAppContext().getSharedPreferences("MY_APP_PER",
                Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public static SPref getInstance() {
        if (instance == null)
            instance = new SPref();
        return instance;
    }

    /*******
     * string value
     */
    public void storeValue(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String retrieveStringValue(String key, String defaultValue) {

        return prefs.getString(key, defaultValue);
    }

    public String retrieveStringValue(String key) {

        return prefs.getString(key, "");
    }

    /*******
     * int value
     */
    public void storeValue(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public int retrieveIntValue(String key) {

        return prefs.getInt(key, -1);
    }

    public int retrieveIntValue(String key, int defaultValue) {

        return prefs.getInt(key, defaultValue);
    }

    /*******
     * float value
     */
    public void storeValue(String key, float value) {
        editor.putFloat(key, value);
        editor.commit();
    }

    public float retrieveFloatValue(String key) {

        return prefs.getFloat(key, -1);
    }

    public float retrieveFloatValue(String key, float defaultValue) {

        return prefs.getFloat(key, defaultValue);
    }

    /*******
     * boolean value
     */
    public void storeValue(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean retrieveBooleanValue(String key) {

        return prefs.getBoolean(key, false);
    }

    public boolean retrieveBooleanValue(String key, boolean value) {

        return prefs.getBoolean(key, value);
    }

    /*******
     * long value
     */
    public void storeValue(String key, long value) {
        editor.putLong(key, value);
        editor.commit();
    }

    public long retrieveLongValue(String key) {

        return prefs.getLong(key, -1);
    }

    public long retrieveLongValue(String key, long value) {

        return prefs.getLong(key, value);
    }


    public void removeAll() {
        editor.clear();
        editor.commit();
    }
}
