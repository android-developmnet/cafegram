package com.ahmad.cafegram.utills;


import com.ahmad.cafegram.R;

import java.util.Date;

public class TimeUtils {

    public static final int SECOND_MILLIS = 1000;
    public static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    public static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    public static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    public static final long WEEK_MILLIS = 7 * DAY_MILLIS;

    public static String getTimeAgoEnNumber(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = new Date().getTime();
        if (time > now || time <= 0) {
            return Utils.getStringResource(R.string.now);
        }

        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return Utils.getStringResource(R.string.now);
        } else if (diff < 2 * MINUTE_MILLIS) {
            return " 1 " + Utils.getStringResource(R.string.min);
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + Utils.getStringResource(R.string.min);
        } else if (diff < 90 * MINUTE_MILLIS) {
            return " 1 " + Utils.getStringResource(R.string.hour);
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + Utils.getStringResource(R.string.hour);
        } else if (diff < 48 * HOUR_MILLIS) {
            return Utils.getStringResource(R.string.yesterday);
        } else if (diff < 7 * DAY_MILLIS) {
            return diff / DAY_MILLIS + Utils.getStringResource(R.string.day);
        } else {
            return diff / WEEK_MILLIS + Utils.getStringResource(R.string.week);
        }
    }

}