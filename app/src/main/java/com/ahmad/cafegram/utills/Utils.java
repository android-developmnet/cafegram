package com.ahmad.cafegram.utills;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Toast;

import com.ahmad.cafegram.R;
import com.ahmad.cafegram.common.RecyclerType;

import java.io.File;


/**
 * Created by ahamdUser on 10/23/2014.
 */
public class Utils {

    private static Context context;

    public static void setupUtils(Context mContext) {
        context = mContext;
    }

    public static Context getAppContext() {
        return context;
    }

    private static boolean isContextInitialized() {
        if (context == null) {
            Log.e("Utils", "you did not set the context, call setupUtils first");
            return false;
        }
        return true;
    }


    public static void showToast(String text, Integer duration) {
        if (!isContextInitialized()) {
            return;
        }
        Toast.makeText(context, text, duration).show();
    }

    public static void showToast(int stringId, Integer duration) {
        if (!isContextInitialized()) {
            return;
        }
        Toast.makeText(context, stringId, duration).show();
    }


    public static boolean isNetworkAvailable() {
        if (!isContextInitialized()) {
            return false;
        }

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static int getColorResource(int resource) {
        return context.getResources().getColor(resource);
    }

    public static String getStringResource(int resource) {
        return context.getResources().getString(resource);
    }

    public static String[] getArrayResource(int resource) {
        return context.getResources().getStringArray(resource);
    }

    public static float getDimensionResource(int resId) {
        if (context == null)
            return 0;
        return context.getResources().getDimension(resId);
    }

    public static int getDimensionInPixelResource(int resId) {
        if (context == null)
            return 0;
        return context.getResources().getDimensionPixelSize(resId);
    }
    private static int getIntegerFromResource(int resId) {
        if (context == null)
            return 0;
        return context.getResources().getInteger(resId);
    }


    public static boolean inArray(int[] ints, int value) {

        for (int i : ints) {
            if (i == value)
                return true;
        }
        return false;
    }
    public static LinearLayoutManager getLayoutManger(Activity activity, RecyclerType recyclerType) {
        LinearLayoutManager layoutManager;
        if (RecyclerType.LIST == recyclerType)
            layoutManager = new LinearLayoutManager(activity);
        else
            layoutManager = new GridLayoutManager(activity, getIntegerFromResource(R.integer.grid_columns));
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        return layoutManager;
    }



    public static void clearCacheFolder() {
        clearRecursive(context.getCacheDir());
    }

    private static void clearRecursive(final File dir) {
        if (dir != null && dir.isDirectory()) {
            try {
                for (File child : dir.listFiles()) {
                    //first delete subdirectories recursively
                    if (child.isDirectory()) {
                        clearRecursive(child);
                    }
                    child.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("deprecation")
    public static void clearCookies() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(context);
            cookieSyncManager.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncManager.stopSync();
            cookieSyncManager.sync();
        }
    }


    public static Drawable getDrawable(int resId) {
        return context.getResources().getDrawable(resId);
    }
}
