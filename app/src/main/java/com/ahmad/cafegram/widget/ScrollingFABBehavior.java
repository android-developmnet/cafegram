package com.ahmad.cafegram.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by ahmad on 1/21/16.
 */
public class ScrollingFABBehavior extends CoordinatorLayout.Behavior<FloatingActionButton> {
    private int toolbarHeight;

    public ScrollingFABBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (Build.VERSION.SDK_INT > 11) {
            final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                    new int[]{android.R.attr.actionBarSize});
            this.toolbarHeight = (int) styledAttributes.getDimension(0, 0);
            styledAttributes.recycle();
        }
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, FloatingActionButton fab, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, FloatingActionButton fab, View dependency) {
        if (Build.VERSION.SDK_INT > 11) {
            if (dependency instanceof AppBarLayout) {
                CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
                int fabBottomMargin = lp.bottomMargin;
                int distanceToScroll = fab.getHeight() + fabBottomMargin;
                float ratio = dependency.getY() / (float) toolbarHeight;
                fab.setTranslationY(-distanceToScroll * ratio);
            }
            return true;
        }
        return false;
    }
}
