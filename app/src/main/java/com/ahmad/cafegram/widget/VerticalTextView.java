package com.ahmad.cafegram.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahmad.cafegram.R;
import com.ahmad.cafegram.utills.Utils;

/**
 * Created by ahmad on 1/22/16.
 */
public class VerticalTextView extends LinearLayout {
    private int firstTextId;
    private int secondTextId;
    private TextView firstTextView;
    private TextView secondTextView;


    public VerticalTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        readAttr(context, attrs);
        init();
    }

    public void init() {
        firstTextView = new TextView(getContext());
        firstTextView.setTextColor(Color.WHITE);
        firstTextView.setGravity(Gravity.CENTER);
        firstTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        setFirstText(firstTextId);
        addView(firstTextView);
        secondTextView = new TextView(getContext());
        secondTextView.setGravity(Gravity.CENTER);
        if (!isInEditMode())
            secondTextView.setTextColor(Utils.getColorResource(R.color.backgroundLight));
        setSecondText(secondTextId);
        addView(secondTextView);

    }

    public void setFirstText(int resId) {
        if (firstTextId != -1)
            firstTextView.setText(resId);
    }

    public void setSecondText(int resId) {
        if (secondTextId != -1)
            secondTextView.setText(resId);
    }

    public void setFirstText(String text) {
            firstTextView.setText(text);
    }

    public void setSecondText(String text) {
            secondTextView.setText(text);
    }

    private void readAttr(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.VerticalText);
        firstTextId = a.getResourceId(R.styleable.VerticalText_firstText, -1);
        secondTextId = a.getResourceId(R.styleable.VerticalText_secondText, -1);
        a.recycle();
    }
}
